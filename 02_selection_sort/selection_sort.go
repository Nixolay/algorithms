package selection_sort

func sort(arr []int) {
	for n := range arr {
		tmp := n
		for i := n; i < len(arr); i++ {
			if arr[tmp] > arr[i] {
				tmp = i
			}
		}

		arr[n], arr[tmp] = arr[tmp], arr[n]
	}
}
