package selection_sort

import (
	"fmt"
	"testing"

	"gitlab.com/Nixolay/tools"
)

func TestSort(t *testing.T) {
	arr := []int{10, 2, 1, 4, 5, 2, 3, 56, 7, 9, 8}
	sort(arr)
	if !tools.Sorted(arr) {
		t.Fatal("no sorted array")
	}
	fmt.Println(arr)
}

// go test -bench=. -benchmem
func BenchmarkSort(b *testing.B) {
	arr := []int{10, 2, 1, 4, 5, 2, 3, 56, 7, 9, 8}
	sort(arr)
	if !tools.Sorted(arr) {
		b.Fatal("no sorted array")
	}
	fmt.Println(arr)
}
