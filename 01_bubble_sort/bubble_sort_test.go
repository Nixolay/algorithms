package bubble_sort

import (
	"testing"

	"gitlab.com/Nixolay/tools"
)

func TestSort(t *testing.T) {
	arr := []int{111, 4, 6, 2, 4, 3, 1, 46, 8, 9, 0}
	sort(arr)
	if !tools.Sorted(arr) {
		t.Fatal("is not sorted array")
	}
}
