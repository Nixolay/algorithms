package bubble_sort

func sort(arr []int) {
	if len(arr) < 2 {
		return
	}

	for n := range arr {
		for i := len(arr) - 1; i >= n; i-- {
			if i == n {
				break
			}

			if arr[i] < arr[i-1] {
				arr[i-1], arr[i] = arr[i], arr[i-1]
			}
		}
	}
}
