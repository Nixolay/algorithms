def sort(arr):
    if len(arr) < 2:
        return arr

    for n in range(len(arr)):
        for i in range(len(arr))[n:][::-1]:
            if arr[i] < arr[i - 1]:
                arr[i-1], arr[i] = arr[i], arr[i-1]
