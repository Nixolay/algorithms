def sort(arr):
    for n in range(len(arr)):
        if n == 0:
            continue

        for i in range(len(arr))[:n][::-1]:
            if arr[n] > arr[i]:
                break

            arr[n], arr[i] = arr[i], arr[n]
            n = i
