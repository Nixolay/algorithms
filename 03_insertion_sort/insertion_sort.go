package insertion_sort

func sort(arr []int) {
	for n := range arr {
		if n == 0 {
			continue
		}

		for i := n - 1; i >= 0; i-- {
			if arr[n] > arr[i] {
				break
			}

			arr[n], arr[i] = arr[i], arr[n]
			n = i
		}
	}
}
