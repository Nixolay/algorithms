package insertion_sort

import (
	"fmt"
	"testing"

	"gitlab.com/Nixolay/tools"
)

func TestSort(t *testing.T) {
	arr := []int{111, 4, 6, 2, 4, 3, 1, 46, 8, 9, 0}
	sort(arr)
	if !tools.Sorted(arr) {
		t.Error("array not sorted")
	}

	fmt.Printf("%#v\n", arr)

}

// go test -bench=. -benchmem
func BenchmarkSort(b *testing.B) {
	arr := []int{111, 4, 6, 2, 4, 3, 1, 46, 8, 9, 0}
	sort(arr)
	if !tools.Sorted(arr) {
		b.Error("array not sorted")
	}

	fmt.Printf("%#v\n", arr)
}
