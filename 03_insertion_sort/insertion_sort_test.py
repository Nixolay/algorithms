import unittest
from insertion_sort import sort


class SortTest(unittest.TestCase):
    def test_sort(self):
        arr = [111, 4, 6, 2, 4, 3, 1, 46, 8, 9, 0]
        sort(arr)
        self.assertEqual(arr, [0, 1, 2, 3, 4, 4, 6, 8, 9, 46, 111])


# reset && python -m unittest -v insertion_sort_test.py
if __name__ == '__main__':
    unittest.main()
